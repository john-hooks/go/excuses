package excuses

import (
	"fmt"
	"math/rand"
	"time"
)

var excuses = []string{
	"they accidentally ate cat food and fear they might die",
	"bats have become tangled in their hair and they cannot get them out",
	"the sobriety device will not allow them to start their car",
	"they had issues getting out of the drive way and now they're too tired",
	"last night did not go well and they do not believe they can perform today",
	"they made it to the parking lot but had an accident in their pants and had to turn around",
	"they have fallen and cannot get up",
	"hiccups have overcome them and they cannot function",
	"back snapped, legs tingling",
	"their legs fell asleep on the toilet, and fell standing up",
	"they accidentally drank mouthwash instead of Gatorade",
	"their cat unplugged the alarm clock",
	"the fortune teller told them they might die if they left the house today",
	"someone has glued their doors and windows shut",
	"they need to go to the hospital to get a candy extracted from their nose",
	"they have accidentally locked themselves inside of the house",
	"they have accidentally put petroleum jelly in their eyes",
	"their children changed all of the clocks in the house",
	"they are currently stuck under the bed",
	"all of their underwear are in the washer",
	"their dog has swallowed their keys and they have to wait for them to come out",
	"they are not sure how the solar eclipse will affect them so it's better if they stay home",
	"they became ill from trying too hard at work yesterday",
	"the puppy pooped in both pairs of work shoes",
}

func NewExcuse(name string) string {
	rand.Seed(time.Now().UnixNano())

	i := rand.Intn(len(excuses))
	excuse := excuses[i]

	message := fmt.Sprintf("%s cannot make it to work today because %s", name, excuse)

	return message

}
